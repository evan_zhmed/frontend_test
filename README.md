![logoblack.gif](https://bitbucket.org/repo/bL8q58/images/2677638869-logoblack.gif)
# README #

This is the repo for testing Zhihui's potential frontend hire.

## What is this repository for? ##

* Frontend Test Repo
* v0.1

## Test Rules ##

* You are tested by trying to finish the following tasks.
* You are scored on both how your final code looks like and how you explain it.
* You may not be able to finish it or encounter some troubles. Don't worry, try your best. Go google it.
* You have 10 days = 240 hours to finish this test. The time starts to count the day you joined this repo.

Good luck!

##Set up##
* Install Meteor.js at [here](https://www.meteor.com/install)
* Create a git repo to store your code.
## Task 1 ##
Finish the React Todo app tutorial at [here](https://www.meteor.com/tutorials/react/creating-an-app)

## Task 2 ##
Finish the Angular Todo app tutorial at [here](https://www.meteor.com/tutorials/angular/creating-an-app)

## Task 3 ##
This one is long, see how far you can go in 10 days at [here](http://www.angular-meteor.com/tutorials/socially/angular2/bootstrapping)

## End ##
Congrats! By arriving here, meaning you've already got the basics to call yourself a modern front-end engineer! Web Development is a rapid evolving community and as long as you keep your mind on the edge, you will make yourself a true master on this!

## Who do I talk to? ##

* Evan thueeliyifan@gmail.com eagles_2f(wechat)

##Copy Right##
All rights reserved for [Zhihui Health Technology](zhmed.cn). Any form of transformation of this repo without approval will be punished by law suit.